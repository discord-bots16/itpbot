from spotipy.oauth2 import SpotifyClientCredentials
import spotipy
import json
import os
from dotenv import load_dotenv

# Load Credentials
load_dotenv()
CLIENT_ID = os.getenv('CLIENT_ID')
CLIENT_SECRET = os.getenv('CLIENT_SECRET')
PLAYLIST_ID = os.getenv('PLAYLIST_ID')
PLAYEDLIST_ID = os.getenv('PLAYEDLIST_ID')

# Set up Spotipy interface
client_credentials_manager = SpotifyClientCredentials(CLIENT_ID, CLIENT_SECRET)
sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)

# Perform the actual GET request (Parameters are (Playlist ID, Fields to retrieve, Next URI (if any), Total number of tracks, # of tracks in this request, Initial offset, Market))
results = sp.playlist_items(PLAYLIST_ID, 'items(track(name, artists(name), duration_ms), added_by(id), added_at), total, next', 100, 0, 'NA')
# Save the results to an array
tracks = results['items']

# Spotify limits playlist_items() requests to 100 tracks per request, so if you ask for the 'next' field, it'll give you the uri for the next 100 tracks
while results['next']: # If 'next' is null, then we've reached the end of the playlist
    # Perform the GET for the next 100 tracks and add them to the end of the tracks array
    results = sp.next(results)
    tracks.extend(results['items'])

# Save the entire array to a file named 'playlist.json'
# TODO: Put this in a database instead
with open('playlist.json', 'w', encoding = 'utf8') as filehandle:
    json.dump(tracks, filehandle, indent = 2, ensure_ascii = False)

print("Finished exporting " + str(len(tracks)) + " items.")